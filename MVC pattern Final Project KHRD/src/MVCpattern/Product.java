package MVCpattern;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import javax.swing.table.DefaultTableModel;
import java.io.FileInputStream;
import java.text.DecimalFormat;

public class Product {
    private String barcode="0";
    private double price = 1.20;
    private String pName = "coca cola";
    private int qty;
    public double amount(int qty, double price){
        this.qty = qty;
        this.price = price;
        return qty*price;
    }
    private FileInputStream fileInputStream;
    private static DefaultTableModel mod = new DefaultTableModel();

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public FileInputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(FileInputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    public String getBarcode() { return barcode; }

    public void setBarcode(String barcode) { this.barcode = barcode; }

    public double getPrice() { return price; }

    public void setPrice(double price) { this.price = price; }

    public String getpName() { return pName; }

    public void setpName(String pName) { this.pName = pName; }

    public Product(){

    }

    public Product(String barcode, String pName, int qty, double price){
        this.barcode = barcode;
        this.pName = pName;
        this.qty = qty;
        this.price = price;
    }

    public Node product() throws Exception{
        //Todo : Add Products Image
        fileInputStream = new FileInputStream("MVC pattern Final Project KHRD/src/MVCpattern/Image/coke-200px.png");
        Image image = new Image(fileInputStream);
        ImageView imageView = new ImageView(image);
        imageView.setFitHeight(160);
        imageView.setFitWidth(180);

        DecimalFormat df = new DecimalFormat("$#,##0.00");
        Label lbName = new Label(pName);
        lbName.setId("lbName");
        Label lbPrice = new Label(df.format(price));
        lbPrice.setId("lbPrice");

        //Todo : Create Buttons
        Button btnCart = new Button("Add to Cart");
        btnCart.getStyleClass().add("btn-buy");
        Button btnCancel = new Button("-");
        btnCancel.setId("btn-cancel");
        Button btnRemove = new Button("Remove");
        btnRemove.setId("btn-remove");

        //Todo : Disable Btn Cancel & Btn Remove
        btnCancel.setVisible(false);
        btnRemove.setVisible(false);

        //Todo : Add Event action on Btn Add to Cart
        btnCart.setOnAction(event -> {
            qty++;
            btnCart.setText("Add to Cart (" + qty + ")");
            btnCancel.setVisible(true);
            btnRemove.setVisible(true);
            System.out.println(qty);
        });

        //Todo : Add Event action on Btn Cancel
        btnCancel.setOnAction(event -> {
            qty--;
            btnCart.setText("Add to Cart (" + qty + ")");
            if(qty == 0){
                btnCancel.setVisible(false);
                btnRemove.setVisible(false);
                btnCart.setText("Add to Cart");
            }
            System.out.println(qty);

        });

        //Todo : Add Event action on Btn Remove
        btnRemove.setOnAction(event -> {
            qty = 0;
            btnCart.setText("Add to Cart");
            btnCancel.setVisible(false);
            btnRemove.setVisible(false);
            System.out.println("qty "+qty);
        });

        //Todo : create a H-box and add Cancel and Remove button in
        HBox crBtn = new HBox(btnCancel, btnRemove);
        crBtn.setSpacing(8);
        crBtn.setAlignment(Pos.CENTER);

        //Todo : create a V-box and add all Items in
        VBox vbox = new VBox(imageView,lbName, lbPrice, btnCart, crBtn);
        vbox.setSpacing(8);

        vbox.getStylesheets().add("MVCpattern/StyleSheet/InterfaceStyle.css");
        vbox.getStyleClass().add("vbox-product");
        vbox.setAlignment(Pos.CENTER);
        vbox.setStyle("-fx-background-radius: 8; -fx-padding: 12; -fx-border-color: black;" +
                " -fx-border-width: 0; -fx-background-color: white;");


        return vbox;
    }
}