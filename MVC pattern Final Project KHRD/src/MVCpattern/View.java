package MVCpattern;

import com.sun.javafx.scene.control.skin.CustomColorDialog;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import jdk.nashorn.internal.runtime.options.Option;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.util.Optional;

public class View {
    private Model model;
    private Controller controller;

    private GridPane gridProducts;
    public View() throws Exception {

        gridProducts = new GridPane();
        gridProducts.setVgap(10);
        gridProducts.setHgap(10);

        int row = 0;
        int col = 0;
        for(int i = 0; i<20; i++){
            Product p = new Product();
            if(col == 4){
                col=0;
                row++;
            }
            col++;
            gridProducts.add(p.product(),col,row);
        }
    }

    public View(Model model, Controller controller){
        this.model = model;
        this.controller = controller;
    }

    private int cartItems = 0;
    private double cartTax = 0;
    private double cartTotal = 0;

    public String toCurrency(double num){
        DecimalFormat df = new DecimalFormat("$#,##0.00");
        String currency = df.format(num);
        return currency;
    }

    public VBox layout() throws Exception {
        //Todo : Create full Layout
        VBox layout = new VBox();
        layout.getStylesheets().add("MVCpattern/StyleSheet/InterfaceStyle.css");
        layout.getChildren().addAll(menubar(), header(), middleLayout());
        return layout;
    }

    public MenuBar menubar(){
        Menu acc = new Menu("Account");
        Menu login = new Menu("LogIn");

        MenuBar menuBar = new MenuBar(acc, login);
        menuBar.setPadding(new Insets(0,0,0,0));
        return menuBar;
    }

    public GridPane gridProduct() throws Exception {
        //Todo : Add Products to gridProducts
//        gridProducts = new GridPane();
//        gridProducts.setVgap(10);
//        gridProducts.setHgap(10);
//
//        int row = 0;
//        int col = 0;
//        for(int i = 0; i<20; i++){
//            if(col == 4){
//                col=0;
//                row++;
//            }
//            col++;
//            gridProducts.add(new VBox(),col,row);
//        }
        return gridProducts;
    }

    public VBox categories(){
        //Todo : Create Check-box
        CheckBox chkMeat = new CheckBox("Meats");
        CheckBox chkVegetable = new CheckBox("Vegetables");
        CheckBox chkFruit = new CheckBox("Fruits");
        CheckBox chkDrink = new CheckBox("Drinks");
        CheckBox chkBread = new CheckBox("Bread/Bakery");
        CheckBox chkPersonCare = new CheckBox("Personal Care");
        chkMeat.setSelected(true);

        //Todo : add items to category
        VBox category = new VBox(chkMeat, chkVegetable, chkFruit, chkBread, chkDrink, chkPersonCare);
        category.setSpacing(8);
        category.setPadding(new Insets(15, 15, 15, 15));
        category.getStyleClass().add("node-style");
        return category;
    }

    public VBox barcode(){
        VBox vBoxBarCode = new VBox();
        vBoxBarCode.setSpacing(15);
        TextField txtBarCode = new TextField();
        txtBarCode.setPromptText("Input Barcode");

        Button btnAdd = new Button("Add to Cart");
        btnAdd.getStyleClass().add("btn-buy");

        vBoxBarCode.getChildren().addAll(txtBarCode, btnAdd);
        vBoxBarCode.setAlignment(Pos.CENTER_RIGHT);
        vBoxBarCode.getStyleClass().add("node-style");
        return vBoxBarCode;
    }

    public HBox cartLeft(){
        //Todo : create label button and add to left side
        Label items = new Label("Items");
        Label tax = new Label("Tax");
        Label total = new Label("Total");
        Node[] nodeLeft = new Node[]{items, tax, total};
        for(Node temp : nodeLeft){ temp.getStyleClass().add("lb-cart-left"); }
        Button btnCart = new Button("Cart");
        btnCart.getStyleClass().add("btn-cart");

        btnCart.setOnAction(event -> {
            try {
                Cart cart = new Cart();
                System.out.println("pressesd");
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        VBox left = new VBox(items, tax, total, btnCart);
        left.setSpacing(8);

        //Todo : create label button and add to right side
        Label rItems = new Label(this.cartItems +"");
        Label rTax = new Label(toCurrency(this.cartTax));
        Label rTotal = new Label(toCurrency(this.cartTotal));
        Node[] nodeRight = new Node[]{rItems, rTax, rTotal};
        for(Node temp : nodeRight){ temp.getStyleClass().add("lb-cart-right"); }
        Button btnCheckOut = new Button("Check out");
        btnCheckOut.getStyleClass().add("btn-cart");

        VBox right = new VBox(rItems, rTax, rTotal, btnCheckOut);
        right.setSpacing(8);
        right.setAlignment(Pos.TOP_RIGHT);

        //Todo : add left & right
        HBox cartLeft = new HBox(left, right);
        cartLeft.setSpacing(8);
        cartLeft.getStyleClass().add("node-style");

        return cartLeft;
    }

    public HBox header() throws FileNotFoundException {
        FileInputStream fileInputStream = new FileInputStream("MVC pattern Final Project KHRD/src/MVCpattern/Image/logo.png");
        Image img = new Image(fileInputStream);
        ImageView imageView = new ImageView(img);
        imageView.setFitWidth(120);
        imageView.setFitHeight(120);
        HBox header = new HBox(imageView);
        header.setStyle("-fx-background-color: white");
        return header;
    }

    public VBox tbProducts() throws Exception {
        //Todo : create Feature Products label
        Label lbProducts = new Label("FEATURED PRODUCTS");
        lbProducts.getStyleClass().add("lb-header");
        lbProducts.setPadding(new Insets(0, 0, 0, 10));

        //Todo : create scroll on products
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(gridProducts);
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scrollPane.setPrefWidth(890);
        scrollPane.setStyle("-fx-background-color: none");

        VBox vBoxProduct = new VBox(lbProducts, scrollPane);
        vBoxProduct.setSpacing(10);

        return vBoxProduct;
    }

    public VBox leftContent(){
        Label lbProducts = new Label("FEATURED PRODUCTS");
        lbProducts.getStyleClass().add("lb-header");
        lbProducts.setPadding(new Insets(0, 0, 0, 10));

        //Todo : add left content
        Label lbCate = new Label("CATEGORIES");
        lbCate.getStyleClass().add("lb-header");

        Label lbBarcode = new Label("BARCODE SCANNER");
        lbBarcode.getStyleClass().add("lb-header");
        Label lbCart = new Label("CART");
        lbCart.getStyleClass().add("lb-header");

        VBox leftContent = new VBox(lbCate, categories(), lbBarcode, barcode(), lbCart, cartLeft());
        leftContent.setSpacing(10);

        return leftContent;
    }

    public HBox middleLayout() throws Exception {
        HBox middleLayout = new HBox(leftContent(), tbProducts());
        middleLayout.setPadding(new Insets(10, 20, 20, 20));
        return middleLayout;
    }

    public void updateCart(){

    }

}
