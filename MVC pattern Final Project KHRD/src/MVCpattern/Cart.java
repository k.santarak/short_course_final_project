package MVCpattern;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public class Cart extends Application {

    public Cart() throws Exception {
        startCart();
    }
    @Override
    public void start(Stage primaryStage) throws Exception {
//        View view = new View();
//
//        VBox vBox = new VBox(view.header(),tableView(),total(), discount(), tax(), SubTotal());
//        vBox.setStyle("-fx-background-color: white");
//        vBox.setSpacing(10);
//        vBox.getStyleClass().add("MVCpattern/StyleSheet/Cart.css");
//        Scene scene = new Scene(vBox, 600, 600);
//        primaryStage.setResizable(false);
//        primaryStage.setScene(scene);
//        primaryStage.show();
    }

    private Stage stage;

    public Stage getStage() {
        return stage;
    }

    public void startCart() throws Exception {
        View view = new View();

        VBox vBox = new VBox(view.header(),tableView(),total(), discount(), tax(), SubTotal());
        vBox.setStyle("-fx-background-color: white");
        vBox.setSpacing(10);
        vBox.getStyleClass().add("MVCpattern/StyleSheet/Cart.css");
        Scene scene = new Scene(vBox, 600, 600);
        stage = new Stage();
        stage.setResizable(false);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    public TableView tableView(){
        TableView tableView = new TableView();
        TableColumn<Product, String> colBarcode = new TableColumn<>("Barcode");
        colBarcode.setCellValueFactory(new PropertyValueFactory<>("barcode"));
        TableColumn<Product, String> colPname = new TableColumn<>("Product Name");
        colPname.setCellValueFactory(new PropertyValueFactory<>("pName"));
        TableColumn<Product, Integer> colQty = new TableColumn<>("Quantity");
        colQty.setCellValueFactory(new PropertyValueFactory<>("qty"));
        TableColumn<Product, Double> colPrice = new TableColumn<>("Price");
        colPrice.setCellValueFactory(new PropertyValueFactory<>("price"));
        TableColumn<Product, Double> colAmount = new TableColumn<>("Amount");
        colAmount.setCellValueFactory(new PropertyValueFactory<>("amount()"));

        colBarcode.setPrefWidth(115);
        colPname.setPrefWidth(140);
        colQty.setPrefWidth(90);
        colPrice.setPrefWidth(100);
        colAmount.setPrefWidth(110);
        TableColumn[]nodes = new TableColumn[]{colBarcode, colPname, colQty, colPrice, colAmount};
        for(int i = 0; i<nodes.length; i++){
            tableView.getColumns().add(nodes[i]);
        }

        tableView.setPrefSize(500, 200);
        tableView.setEditable(false);
        tableView.getStylesheets().add("MVCpattern/StyleSheet/InterfaceStyle.css");
        tableView.setPadding(new Insets(0, 20, 0, 20));

        for (int i = 0; i<10; i++){
            Product p = new Product();
            p.setBarcode("1200245");
            p.setpName("Coca Cola");
            p.setQty(100);
            p.setPrice(10);
            tableView.getItems().add(p);
        }
        return tableView;
    }

    public void txtCartStyle(TextField textField, String prompText){
        textField.setEditable(false);
        textField.setPromptText(prompText);
        textField.getStyleClass().add("txtCart");
    }

    public void hBoxStyle(HBox hBox){
        hBox.getStylesheets().add("MVCpattern/StyleSheet/InterfaceStyle.css");
        hBox.setSpacing(20);
        hBox.setAlignment(Pos.BASELINE_RIGHT);
        hBox.setPadding(new Insets(0, 20, 0 ,0));
        hBox.getStylesheets().add("MVCpattern/StyleSheet/InterfaceStyle.css");
    }

    public HBox tax(){
        Label lbTax = new Label("Tax");
        TextField txtTax = new TextField();
        txtCartStyle(txtTax, "Tax");
        HBox hBox = new HBox(lbTax, txtTax);
        hBoxStyle(hBox);
        return hBox;
    }

    public HBox total(){
        Label lbTotal = new Label("Total");
        TextField txtTotal = new TextField();
        txtCartStyle(txtTotal, "Total");
        HBox hBox = new HBox(lbTotal, txtTotal);
        hBoxStyle(hBox);
        return hBox;
    }

    public HBox discount(){
        Label lbDis = new Label("Discount");
        TextField txtDis = new TextField();
        txtCartStyle(txtDis, "Discount");
        HBox hBox = new HBox(lbDis, txtDis);
        hBoxStyle(hBox);
        return hBox;
    }

    public HBox SubTotal(){
        Button btnCheck = new Button("Check out");
        btnCheck.getStyleClass().add("btn-cart");
        TextField txtTotal = new TextField();
        txtCartStyle(txtTotal, "Sub Total");
        HBox hBox = new HBox(btnCheck, txtTotal);
        hBoxStyle(hBox);
        return hBox;
    }

}
